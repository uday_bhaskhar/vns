var generator = require('generate-password');
var twilio = require('twilio');

var accountSid = 'AC36139b1bf1a9f54241468103cf43fc9a';
var authToken = '0c1c874aa9fa5a3772c42844210e133a';

var client = new twilio(accountSid, authToken);

const passwordSender = {
  generatePassword: () => {
    var password = generator.generate({
        length: 10,
        numbers: true
    });

    return password;
  },

  sendSms: (password) => client.messages.create({
      body: `Hi, I have added you to my vehicle tracking system. Please sign in with this password:${password}`,
      to: '9787752856',
      from: '+1 256-291-0786 '
    })
}

module.exports = passwordSender;
